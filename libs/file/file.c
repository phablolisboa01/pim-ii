#include <stdio.h>
#include <string.h>
#include <ctype.h>

int verificaArquivo(char path[]){ 
    FILE *fp;
    fp=fopen(path,"r");
    if(fp){
        return 1;
    }else{
        return 0;
    }

}

void insert(char arquivo[], char conteudo[]){
    FILE *arquivoP = fopen(arquivo, "a+");
    
    fprintf(arquivoP, "%s\n", conteudo);
    fclose(arquivoP);

}
int find(char arquivo[], char conteudo[], char *result[]){
     
    FILE *arquivoP = fopen(arquivo, "r");
    char str[(strlen(conteudo))], aux[(strlen(conteudo))], aux2[(strlen(conteudo))],id[3];
    int i = 0, x = 0;

    for(i = 0; conteudo[i]; i++){
       aux2[i] = tolower(conteudo[i]);
    }

    if(arquivoP == NULL){
        exit(1);
    }
    else{
        while((fscanf(arquivoP,"%[^\n]\n", &str))!= EOF){
            i = 0;
            while (str[i] != ' '){ i++; }   

            x = 0;
            i++;
            for (i; i <= strlen(str); i++)
            {
                aux[x] = tolower(str[i]);
                x++;
            }

            i = 0;
            if(strcmp(aux2, aux) == 0){
                while (str[i] != '.')
                {
                    id[i] = str[i];
                    i++;
                }
                strcpy(result, id);
                fclose(arquivoP);
                return 0;       
            }
        }
       return 1; 
    }
}

int findById(char arquivo[], char id[] , char *result[]){
     
    FILE *arquivoP = fopen(arquivo, "r");
    char str[50], idFinded[4];
    int i = 0;

    if(arquivoP == NULL){
        exit(1);
    }
    else{
        while((fscanf(arquivoP,"%[^\n]\n", &str))!= EOF){
            i = 0;

            while(str[i] != '.'){
                idFinded[i] = str[i];
                i++;
            }    
            if( (strcmp(idFinded, id))==0){
                   strcpy(result, str);
                   fclose(arquivoP);
                   return 0;
            }            
        }
        return 1;
        
    }
}

int idSearch(char arquivo[], int id, char *result[]){
    FILE *arquivoP = fopen(arquivo, "r");
    char str[50], auxStr[50];
    int i = 1, x = 0;

    if(arquivoP == NULL){
        exit(1);
    }
    else{
        while((fscanf(arquivoP,"%[^\n]\n", &str))!= EOF){
           char *token = strtok(str, " ");
            if( i == id){
                //Apenas para retornar o registro limpo, sem o ID
                while (token != NULL) { 
                    strcat(auxStr, token);
                   
                    x++;
                    token = strtok(NULL, " "); 
                } 

                strcpy(result, auxStr);
                fclose(arquivoP);
                return 0;
            }       

            i++;     
        }
        return 1;
        
    }
}

int getLastId(char arquivo[]){
    FILE *arquivoP = fopen(arquivo, "r");
    char str[50];
    int i = 0;

    if(arquivoP == NULL){
        exit(1);
    }
    else{
        while((fscanf(arquivoP,"%[^\n]\n", &str))!= EOF){
            i++;
        }
        return i;
    }

}


int apagar(char arquivo[], char id[]){
    FILE *arquivoP = fopen(arquivo, "r");
    char str[50], aux[200][50];
    int i = 0, x = 0;    
     
    while((fscanf(arquivoP,"%[^\n]\n", &str))!= EOF){
        if((i+1 ) != id){
            strcpy(aux[x],str);
            x++;
        }else
        i++;
    }
    fclose(arquivoP);
    remove(arquivo);

    for (x = 0; x <= i; x++){
        insert(arquivo, aux[x]);
    }

    return 0;
}


int update(char arquivo[], int id, char newContent[]){
    FILE *arquivoP = fopen(arquivo, "r");
    char str[50], aux[200][50];
    int i = 0;    
     
    while((fscanf(arquivoP,"%[^\n]\n", &str))!= EOF){

        if((i+1 ) == id){
            sprintf(aux[i], "%i. ", (i+1));
            strcat(aux[i], newContent);
        }else{
            strcpy(aux[i],str);
        }

        i++;
    }
    fclose(arquivoP);
    remove(arquivo);

    for (int x = 0; x <= i; x++)
    {
        insert(arquivo, aux[x]);
    }

    return 0;
}
