#include <stdio.h>
#include <stdlib.h>

char *logOrCad[]={
        "1. Já Sou Cadastrado!",
        "0. Sair",
        NULL
};

char Menu(char *opcoes[], char message[]){
    int i = 0;
    char ch;

    clrscr();
    header();
    printf("\n\n\t\t%s\n", message);
    puts("\t\t-------------------------------------------\n\n");
    puts("\t\tSelecione uma opção, digitando o número correspondente:");

    while(1){
        /*CLS */
        printf("\n\n\n");
            for(i=0; opcoes[i]!=NULL; i++){
                printf("\t\t%s\n\n",opcoes[i]);

            }
               
            printf("\n\n\t\tOpcao: ");
            ch = getchar(); 
            while ((getchar()) != '\n'); 
            // fflush(stdin);

            for(i=0; opcoes[i]!= NULL; i++){
                if(opcoes[i][0]==ch){
                    fflush(stdin);
                    return ch;
                } 
            }
    }
}

void Router(char selected, void (* *funcs)(), void (*voltar)()){
    int  selectedInt = selected - '0'; //Convertendo CHAR em INT

   switch (selectedInt)
    {
    case 0:
        exit(0);
        break;
    case 9:
        (*voltar)();
        break;
    default:
        (*funcs[selectedInt])();
    }
}
