#include <stdio.h>
int countLines(char arquivo[]){
    FILE *file = fopen(arquivo, "r");
    char straux[100];
    int i = 0;
    while((fscanf(file,"%[^\n]\n", &straux))!= EOF)
        i++;

    return i+1;
}

int getNumeroVendas(char arquivo[], int id, int casa){
    FILE *file = fopen(arquivo, "r");
    char *str, *result[10];
    int x = 0;

    memset(&result, 0, sizeof(result));

    idSearch(arquivo, id, result);
    // printf("%s\n",result);

    str = strtok(result, "|");
    while(str != NULL)
    {           
        if(x == casa)   
            return atoi(str);
        str = strtok(NULL, "|");            
        x++;
    }


}


void updateVendas(char arquivo[], int id, int casa){
    FILE *file = fopen(arquivo, "r");
    int i = 0, x = 0;
    char registro[50], straux[50], *str, newVenda[10];

    sprintf(registro, "%s", "|");
    sprintf(newVenda, "%i", (getNumeroVendas(arquivo, id, casa)+1));

    while((fscanf(file,"%[^\n]\n", &straux))!= EOF){
       if ((i+1) == id)
       {
            str = strtok(straux, "|");

            while(str != NULL)
            {

                if(x > 0){

                    if(x == casa){
                        strcat(registro, newVenda);
                    }else{
                        strcat(registro, str);
                    }

                    strcat(registro, "|");
                    
                }


                str = strtok(NULL, "|");
                x++;
                
            }

            update(arquivo, id, registro);
       }
       

       i++;
   }
    fclose(file);
}