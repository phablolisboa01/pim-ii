#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void mainEmployerFunc();

char *loginTypes[]={
        "1. Administrador",
        "2. Empregado",
        "0. Sair",
        NULL
};

int verifyLogin(int type, char nome[], char senha[]){
    char *nomeFind[3], *senhaFind[50], cmpSenha[50];

    int i = 0;
    memset(nomeFind, 0, sizeof(nomeFind));
    memset(cmpSenha, 0, sizeof(cmpSenha));

    for(int i = 0; senha[i]; i++){
        senha[i] = tolower(senha[i]);
    }

    if(type == 1){    
        if(find("db/admin/name.dat", nome, nomeFind) != 0)
            return 1;
        
        if(findById("db/admin/pass.dat", nomeFind ,senhaFind) != 0)
            return 1;
        
        strcpy(cmpSenha,(strchr(senhaFind, ' ') + 1));
        
        for(int i = 0; cmpSenha[i]; i++){
            cmpSenha[i] = tolower(cmpSenha[i]);
        }
        
        if(strcmp(senha, cmpSenha) == 0)
            return 0;      
    }    

    if(find("db/employer/name.dat", nome, nomeFind) != 0)
        return 1;
    
    if(findById("db/employer/pass.dat", nomeFind ,senhaFind) != 0)
        return 1;
    
    strcpy(cmpSenha,(strchr(senhaFind, ' ') + 1));
    
    for(int i = 0; cmpSenha[i]; i++){
        cmpSenha[i] = tolower(cmpSenha[i]);
    }

    if(strcmp(senha, cmpSenha) == 0){
        return 0;    
    }
    return 1; 
}

int login(char type[]){
    char nome[50], senha[50];
    int i = 0;
    do
    {
        clrscr();
        header();

        if(i > 0)
            printf("\n\n\t\tSenha ou Nome incorreto, por favor, tente novamente:");
        

        printf("\n\n\t\t\t\tNome: ");
        gets(nome);

        printf("\n\t\t\t\tSenha: ");
        gets(senha);

        if(strcmp(type, "admin")== 0){
            if(verifyLogin(1, nome, senha) == 0){
                mainAdminFunc();
                return 0;
            }
        }

        if(verifyLogin(2, nome, senha) == 0){
            mainEmployerFunc();
            return 0;
        }

        i++;
    } while ((verifyLogin(1, nome, senha) != 0) || (verifyLogin(1, nome, senha) != 0));
    
}


void loginAdm(){
    login("admin");
}
void loginFranq(){
    login("employer");
}


void loginTypesFunc(){
    

    char op = Menu(loginTypes, "Como você deseja fazer o login?");

    void *logins[3];
    logins[1] = loginAdm;
    logins[2] = loginFranq;

    Router(op, logins, exit);    
}
