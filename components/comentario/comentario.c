#include <stdio.h>


void fazerComentario(){
    char *top[]=
    {
        "1. Realizar outro Comentário",
        "2. Voltar",
        NULL
    };
    char nameCliente[50], comentario[1000], registro[2000];
    clrscr();
    header();


    printf("\n\t\tRealizar comentário:");

    printf("\n\n\n\t\tNome do Cliente: ");
    gets(nameCliente);
    printf("\n\t\tComentário: ");
    gets(comentario);


    sprintf(registro, "%i. |%s |%s", getLastId("db/comentarios/comentarios.dat")+1, nameCliente, comentario);
    insert("db/comentarios/comentarios.dat",registro);

    char op; op = Menu(top, "Comentário  Realizado com sucesso");

    op == '1'? fazerComentario(): mainAdminFunc();

}