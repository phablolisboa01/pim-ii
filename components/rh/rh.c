
void rh();
void mainAdminFunc();

void cadFunc(){
    char registro[50];
    struct funcionarios
    {
        char nome[50];
        char funcao[20];
        char salario[10];

    }funcionario;
    
    clrscr();
    header();

    printf("\n\n\t\t\t\tNome: ");
    gets(funcionario.nome);

    printf("\n\n\t\t\t\tFunção: ");
    gets(funcionario.funcao);

    printf("\n\n\t\t\t\tSalário: R$ ");
    gets(funcionario.salario);

    sprintf(registro, "%i", getLastId("db/rh/employers.dat")+1);
    strcat(registro, ". |");
    strcat(registro, funcionario.nome);
    strcat(registro, " |");
    strcat(registro, funcionario.funcao);
    strcat(registro, " |");
    strcat(registro, funcionario.salario);

    insert("db/rh/employers.dat", registro);

    
    char op = Menu(yesOrNo, "Funcionário Cadastrado com sucesso! Deseja Cadastrar outro?");
    void *funcs[3];
    funcs[1] = cadFunc;
    funcs[2] = rh;

    Router(op, (funcs), mainAdminFunc);  
}
void listFunc(){
    char straux[50],*str;
    int space = 0, x = 0;
    FILE *arquivo = fopen("db/rh/employers.dat", "r");

    clrscr();
    header();
    printf("\n\n\t\t\t|      Nome     |     Função    |     Salário   |\n");
    
    while((fscanf(arquivo,"%[^\n]\n", &straux))!= EOF){
        str = strtok(straux, "|");
        printf("\t\t  ");
        x = 0;
        while(str != NULL)
        {
            if(x == 0){
                printf(" %s  |", str);
            }else if(x == 3){
                printf("R$ %-12s|", str);
            }else{
                printf("%-15s |", str);

            }
            str = strtok(NULL, "|");
            x++;
        }
        printf("\n");
    }
    
    
}

void listAndOpFunc(){
    char op;
    listFunc();
    printf("\n\n\t\t\t Voltar(S/N): ");
    op = getchar();

    if (tolower(op) == 's'){
        rh();
    }
    else{
        listAndOpFunc();
    }
    
}

void editFunc(){
    char registro[50];
    struct funcionarios
    {
        char nome[50];
        char funcao[20];
        char salario[10];

    }funcionario;

    
    int id = 0;
    listFunc();
    printf("\n\n\t\tDigite o Número referente ao item que deseja alterar: ");
    scanf("%i", &id);
    
    clrscr();
    header();
    while ((getchar()) != '\n'); 

    printf("\n\n\t\t\t\tNome do Funcionário: ");
    gets(funcionario.nome);

    printf("\n\n\t\t\t\tFunção: ");
    gets(funcionario.funcao);

    printf("\n\n\t\t\t\tSalário : R$ ");
    gets(funcionario.salario);

    sprintf(registro, "|");
    strcat(registro, funcionario.nome);
    strcat(registro, " |");
    strcat(registro, funcionario.funcao);
    strcat(registro, " |");
    strcat(registro, funcionario.salario);

    update("db/rh/employers.dat", id, registro);
    
    
    char op = Menu(yesOrNo, "Atualizar outro Funcionário(S/N): ");
    void *funcs[3];
    funcs[1] = editFunc;
    funcs[2] = rh;

    Router(op, (funcs), mainAdminFunc);  
    
}
void deleteFunc(){
    int id = 0;

    listFunc();
    printf("\n\n\t\tDigite o Número referente ao Funcionário que deseja excluir: ");
    scanf("%i", &id);
    update("db/rh/employers.dat", id, "----------------------------------------------");
    char op = Menu(yesOrNo, "Excluir outro Funcionário(S/N): ");
    void *funcs[3];
    funcs[1] = deleteFunc;
    funcs[2] = rh;
    Router(op, (funcs), mainAdminFunc);  
}