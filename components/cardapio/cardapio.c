#include <stdio.h>
void estoque();
void cardAndProducts();

void insertIngredients(){
    int qtd = 0;

    clrscr();
    header();
    printf("\n\n\t\tQuantos Ingredientes esse prato Terá?: ");
    scanf("%i", &qtd);

    int ingredients[qtd];

    char list[(qtd*2)+3], aux[3];
    sprintf(list, "%i. ", getLastId("db/pratos/ingredients.dat")+1);

    for (int i = 0; i < qtd; i++)
    {
        clrscr();
        header();
        listProducts();
        printf("\n\n\t\tAdicione o %i° ingrediente, digitando seu número: ", i+1);
        scanf("%i", &ingredients[i]);
        sprintf(aux, "%i", ingredients[i]);
        strcat(list, aux);
        strcat(list, " ");

    }
    
    insert("db/pratos/ingredients.dat", list);
}

void cadPrato(){ 
    char registro[50];
    int i = 0;
    struct pratos
    {
        char nome[50];
        char tamanho[5];
        char preco[5];

    }prato;
    
    clrscr();
    header();

    printf("\n\n\t\t\t\tQual prato: ");
    gets(prato.nome);

    printf("\n\n\t\t\t\tTamanho(M/G/GG): ");
    gets(prato.tamanho);

    printf("\n\n\t\t\t\tPreço: R$ ");
    gets(prato.preco);

    insertIngredients();


    sprintf(registro, "%i", getLastId("db/pratos/pratos.dat")+1);
    strcat(registro, ". |");
    strcat(registro, prato.nome);
    strcat(registro, " |");
    strcat(registro, prato.tamanho);
    strcat(registro, " |");
    strcat(registro, prato.preco);
    strcat(registro, " |");
    strcat(registro, "0");

    insert("db/pratos/pratos.dat", registro);

    
    char op = Menu(yesOrNo, "Prato Cadastrado com sucesso! Deseja Cadastrar outro?");
    void *funcs[3];
    funcs[1] = cadPrato;
    funcs[2] = cardAndProducts;

    Router(op, (funcs), mainAdminFunc);  
}

void listPrato(){
    char straux[50],*str, *ingredients[50];
    int x = 0;
    FILE *arquivo = fopen("db/pratos/pratos.dat", "r");

    clrscr();
    header();
    printf("\n\n\t\t\t|     Prato      |   Tamanho     |     Preço     |     Vendas     |\n");
    
    while((fscanf(arquivo,"%[^\n]\n", &straux))!= EOF){
        str = strtok(straux, "|");
        printf("\t\t  ");
        x = 0;
        while(str != NULL)
        {
            if(x == 0){
                printf("  %s |", str);
            }else if(x == 3){
                printf("R$ %-12s|", str);
            }else{
                printf("%-15s|", str);

            }
            str = strtok(NULL, "|");

            x++;
        }
        printf("\n");
    }  
}

void listAndOpCardapio(){
    char op;
    listPrato();
    printf("\n\n\t\t\t Voltar(S/N): ");
    op = getchar();

    if (tolower(op) == 's'){
        cardAndProducts();
    }
    else{
        listAndOpCardapio();
    }
}

void editPrato(){ 
     char registro[50];

    struct pratos
    {
        char nome[50];
        char tamanho[5];
        char preco[5];

    }prato;
    
    int id = 0;
    listPrato();
    printf("\n\n\t\tDigite o Número referente ao item que deseja alterar: ");
    scanf("%i", &id);
    
    clrscr();
    header();
    while ((getchar()) != '\n'); 

    printf("\n\n\t\t\t\tNovo nome do Prato: ");
    gets(prato.nome);

    printf("\n\n\t\t\t\tTamanho: ");
    gets(prato.tamanho);

    printf("\n\n\t\t\t\tPreço: R$ ");
    gets(prato.preco);

    sprintf(registro, "|");
    strcat(registro, prato.nome);
    strcat(registro, " |");
    strcat(registro, prato.tamanho);
    strcat(registro, " |");
    strcat(registro, prato.preco);

    update("db/pratos/pratos.dat", id, registro);

     int qtd = 0;

    clrscr();
    header();
    printf("\n\n\t\tQuantos Ingredientes esse prato Terá?: ");
    scanf("%i", &qtd);

    int ingredients[qtd];

    char list[(qtd*2)+3], aux[3];

    for (int i = 0; i < qtd; i++)
    {
        clrscr();
        header();
        listProducts();
        printf("\n\n\t\tAdicione o %i° ingrediente, digitando seu número: ", i+1);
        scanf("%i", &ingredients[i]);
        sprintf(aux, "%i", ingredients[i]);
        strcat(list, aux);
        strcat(list, " ");

    }
    
    update("db/pratos/ingredients.dat",id, list);
    
    
    char op = Menu(yesOrNo, "Atualizar outro Prato(S/N): ");
    void *funcs[3];
    funcs[1] = editPrato;
    funcs[2] = cardAndProducts;

    Router(op, (funcs), mainAdminFunc);  
}
void removePrato(){
    int id = 0;

    listPrato();
    printf("\n\n\t\tDigite o Número referente ao item que deseja excluir: ");
    scanf("%i", &id);
    update("db/pratos/pratos.dat", id, "--------------------------------------------");
    update("db/pratos/ingredients.dat", id, "--------------------------------------------");
    char op = Menu(yesOrNo, "Excluir outro Produto(S/N): ");
    void *funcs[3];
    funcs[1] = removePrato;
    funcs[2] = cardAndProducts;
    Router(op, (funcs), mainAdminFunc);  
}