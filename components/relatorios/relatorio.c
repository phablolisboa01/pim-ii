#include "stdio.h";
#include <time.h>;
// "1. Pizzas Mais Vendidas",
//     "2. Franquia que mais vende",
//     "3. Total lucrado",
//     "4. Media de Lucro Mensal",
//     "9. Voltar",
//     "0. Sair",

void maisVendida(){
    struct tm * tm;
    time_t t;
    time(&t);
    tm = localtime(&t);


    FILE *arquivo = fopen("db/pratos/pratos.dat", "r");

    char nameRelatorio[100] = {}, aux[4];
    sprintf(aux, "%i", tm->tm_mday);
    strcat(nameRelatorio, "db/relatorios/pizza/maisvende-");
    strcat(nameRelatorio, aux);
    strcat(nameRelatorio, "-");
    sprintf(aux, "%i", tm->tm_mon+1);
    strcat(nameRelatorio, aux);
    strcat(nameRelatorio, ".dat");


    char straux[50], *str, count, nomeAux[40], nome[40], op, anotherAux[500];
    int i = 0, x = 0, vendas[countLines("db/pratos/pratos.dat")],indice = 0,
     indiceMaior = 0, maior = 0;

    while((fscanf(arquivo,"%[^\n]\n", &straux))!= EOF){

        i = 0;
        str = strtok(straux, "|");

        while(str != NULL){
            if (i == 1)
                strcpy(nomeAux, str);

            if(i == 4){
                if(atoi(str) > maior){
                    maior = atoi(str);
                    indiceMaior = indice;
                    strcpy(nome, nomeAux);            
                }
              
            }

            str = strtok(NULL, "|");
            i++;
        }

        indice++;


    }   

    remove(nameRelatorio);
    sprintf(anotherAux, "Pizza de maior venda:%s\t\tNúmero de vendas: %i\n",nome, maior);
    insert(nameRelatorio, anotherAux);
    sprintf(anotherAux, "Relatório gerado em %i/%i/%i", tm->tm_mday, tm->tm_mon, tm->tm_year+1900);
    insert(nameRelatorio, anotherAux);    

    listPrato();
    printf("\n\n\n\t\tPizza de maior venda: %s\n", nome);
    printf("\t\tNúmero de vendas: %i\n", maior);
    printf("\n\n\t\t\t Voltar(S/N): ");
    op = getchar();

    if (tolower(op) == 's'){
        relatorios();
    }
    else{
        maisVendida();
    }

}
void franqMaisVende(){
    struct tm * tm;
    time_t t;
    time(&t);
    tm = localtime(&t);    

    FILE *arquivo = fopen("db/franquias/franquias.dat", "r");

    char nameRelatorio[100] = {}, aux[4];
    sprintf(aux, "%i", tm->tm_mday);
    strcat(nameRelatorio, "db/relatorios/franquia/maisvende-");
    strcat(nameRelatorio, aux);
    strcat(nameRelatorio, "-");
    sprintf(aux, "%i", tm->tm_mon+1);
    strcat(nameRelatorio, aux);
    strcat(nameRelatorio, ".dat");


    char straux[50], *str, count, nomeAux[40], nome[40], op, anotherAux[500];
    int i = 0, x = 0, vendas[countLines("db/franquias/franquias.dat")],indice = 0,
     indiceMaior = 0, maior = 0;

    while((fscanf(arquivo,"%[^\n]\n", &straux))!= EOF){

        i = 0;
        str = strtok(straux, "|");

        while(str != NULL){
            if (i == 1)
                strcpy(nomeAux, str);

            if(i == 3){
                if(atoi(str) > maior){
                    maior = atoi(str);
                    indiceMaior = indice;
                    strcpy(nome, nomeAux);            
                }
              
            }

            str = strtok(NULL, "|");
            i++;
        }

        indice++;


    }   
    remove(nameRelatorio);
    sprintf(anotherAux, "Franquia Que Mais Vendeu:%s\t\tNúmero de vendas: %i\n",nome, maior);
    insert(nameRelatorio, anotherAux);
    sprintf(anotherAux, "Relatório gerado em %i/%i/%i", tm->tm_mday, tm->tm_mon, tm->tm_year+1900);
    insert(nameRelatorio, anotherAux);    

    listFranq();
    printf("\n\n\n\t\tFranquia Que Mais Vendeu: %s\n", nome);
    printf("\t\tNúmero de vendas: %i\n", maior);
    printf("\n\n\t\t\t Voltar(S/N): ");
    op = getchar();

    if (tolower(op) == 's'){
        relatorios();
    }
    else{
        franqMaisVende();
    }
}
void lucro(){
    struct tm * tm;
    time_t t;
    time(&t);
    tm = localtime(&t);    

    FILE *pratos = fopen("db/pratos/pratos.dat", "r");
    FILE *estoque = fopen("db/stock/products.dat", "r");
    FILE *empregado = fopen("db/rh/employers.dat", "r");

    char nameRelatorio[100] = {}, aux[4];
    sprintf(aux, "%i", tm->tm_mday);
    strcat(nameRelatorio, "db/relatorios/geral/lucro-");
    strcat(nameRelatorio, aux);
    strcat(nameRelatorio, "-");
    sprintf(aux, "%i", tm->tm_mon+1);
    strcat(nameRelatorio, aux);
    strcat(nameRelatorio, ".dat");

    char strPrato[50], strEstoque[50], strEmpregado[50], *str,op, anotherAux[500];

    int i = 0, totalVendas = 0, preco = 0, totalCompras = 0, totalSalarios = 0;

    while((fscanf(pratos,"%[^\n]\n", &strPrato))!= EOF){
        i = 0;
        str = strtok(strPrato, "|");

        while(str != NULL){
            if(i == 3)
                preco = atof(str);
            if(i == 4){
                totalVendas += (atoi(str))*preco;
                
            }

            str = strtok(NULL, "|");
            i++;
        }

    }
    i=0;
    while((fscanf(estoque,"%[^\n]\n", &strEstoque))!= EOF){
        i = 0;
        str = strtok(strEstoque, "|");

        while(str != NULL){
            if(i == 2)
                preco = atof(str);
            if(i == 3){
                totalCompras += (atoi(str))*preco;
                
            }
            str = strtok(NULL, "|");
            i++;
        }
    }


    i=0;
    while((fscanf(empregado,"%[^\n]\n", &strEmpregado))!= EOF){
        i = 0;
        str = strtok(strEmpregado, "|");

        while(str != NULL){
            if(i == 3){
                totalSalarios += atoi(str);
                
            }
            str = strtok(NULL, "|");
            i++;
        }
    }
    remove(nameRelatorio);
    sprintf(anotherAux, "\n\t\t\tTotal em estoque: %i\n\t\t\tDespesa total com RH: %i\n\t\t\tGanho total com vendas: %i\n\t\t\tLucro Total Final: %i\n",totalCompras, totalSalarios,totalVendas, (totalVendas - totalCompras - totalSalarios));
    insert(nameRelatorio, anotherAux);
    sprintf(anotherAux, "Relatório gerado em %i/%i/%i", tm->tm_mday, tm->tm_mon, tm->tm_year+1900);
    insert(nameRelatorio, anotherAux);    

    clrscr();
    header();
    printf("\n\t\t\tTotal em estoque: %i\n", totalCompras);
    printf("\t\t\tDespesa total com RH: %i\n", totalSalarios);
    printf("\t\t\tGanho total com vendas: %i\n", totalVendas);
    printf("\t\t\tLucro Total Final: %i\n", (totalVendas - totalCompras - totalSalarios));

    printf("\n\n\t\t\t Voltar(S/N): ");
    op = getchar();
    if (tolower(op) == 's'){
        relatorios();
    }
    else{
        lucro();
    } 

}
void mediaLucro(){ puts("teste");}
