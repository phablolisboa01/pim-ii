

char* opsaAdmin[] = {
                 "1. Controle de Estoque",
                 "2. Recursos Humanos",
                 "3. Cardápio e Produtos",
                 "4. Franquias",
                 "5. Relatórios",
                 "0. Sair",
                 NULL
            };

char* opsEstoque[] = {
    "1. Cadastrar Produto",
    "2. Listar Produtos",
    "3. Utualizar Produtos",
    "4. Excluir Produto",
    "9. Voltar",
    "0. Sair",
    NULL
};


char* opsRH[] = {
    "1. Cadastrar Funcionário",
    "2. Listar Funcionários",
    "3. Editar Perfil de Funcionário",
    "4. Excluir Funcionários",
    "9. Voltar",
    "0. Sair",
    NULL
};
char* opscardAndProducts[] = {
    "1. Cadastrar Prato",
    "2. Listar Pratos disponíveis",
    "3. Editar Pratos disponíveis",
    "4. Excluir Pratos",
    "9. Voltar",
    "0. Sair",
    NULL
};

char* opsFranquias[] = {
    "1. Cadastrar Franquia",
    "2. Listar Franquias",
    "3. Editar Franquia",
    "4. Excluir Franquia",
    "9. Voltar",
    "0. Sair",
    NULL
};

char* opsrelatorios[] = {
    "1. Pizzas Mais Vendidas",
    "2. Franquia que mais vende",
    "3. Total lucrado",
    "4. Media de Lucro Mensal",
    "9. Voltar",
    "0. Sair",
    NULL
};

void mainAdminFunc();

void estoque(){
    char op = Menu(opsEstoque, "O que você deseja fazer?");
    void *funcs[5];
    funcs[0] = exit;
    funcs[1] = cadProducts;
    funcs[2] = listAndOp;
    funcs[3] = updateProduct;
    funcs[4] = removeProducts;

    Router(op, (funcs), mainAdminFunc);  

}

void rh(){
    char op = Menu(opsRH, "O que você deseja fazer?");
    void *funcs[5];
    funcs[0] = exit;
    funcs[1] = cadFunc;
    funcs[2] = listAndOpFunc;
    funcs[3] = editFunc;
    funcs[4] = deleteFunc;

    Router(op, (funcs), mainAdminFunc);  


}
void cardAndProducts(){      
    char op = Menu(opscardAndProducts, "O que você deseja fazer?");
    void *funcs[5];
    funcs[0] = exit;
    funcs[1] = cadPrato;
    funcs[2] = listAndOpCardapio;
    funcs[3] = editPrato;
    funcs[4] = removePrato;

    Router(op, (funcs), mainAdminFunc);  
}

void franquias(){
    char op = Menu(opsFranquias, "O que você deseja fazer?");
    void *funcs[5];
    funcs[0] = exit;
    funcs[1] = createFranq;
    funcs[2] = listAndOpFranq;
    funcs[3] = editFranq;
    funcs[4] = deleteFranq;

    Router(op, (funcs), mainAdminFunc);  
}

void relatorios(){
    char op = Menu(opsrelatorios, "O que você deseja fazer?");
    void *funcs[5];
    funcs[0] = exit;
    funcs[1] = maisVendida;
    funcs[2] = franqMaisVende;
    funcs[3] = lucro;
    funcs[4] = mediaLucro;

    Router(op, (funcs), mainAdminFunc);

}


void mainAdminFunc(){    
    char op = Menu(opsaAdmin, "O que você deseja fazer?");
    void *funcs[5];
    funcs[0] = exit;
    funcs[1] = estoque;
    funcs[2] = rh;
    funcs[3] = cardAndProducts;
    funcs[4] = franquias;
    funcs[5] = relatorios;

    Router(op, (funcs), exit);  

}