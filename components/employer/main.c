char* opsEmployer[] = {
                 "1. Registrar Pedido",
                 "2. Registrar Comentário",
                 "9. Voltar",
                 "0. Sair",
                 NULL
            };


void mainEmployerFunc(){
    char op = Menu(opsEmployer, "O que você deseja fazer?");
    void *funcs[3];
    funcs[0] = exit;
    funcs[1] = fazerPedido;
    funcs[2] = fazerComentario;


    Router(op, (funcs), exit); 

}