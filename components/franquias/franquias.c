void franquias();

void createFranq(){
    char registro[50];
    struct franquias
    {
        char nome[50];
        char gerente[50];
        int vendas[5];

    }franquia;
    
    clrscr();
    header();

    printf("\n\n\t\tQual Nome(Identificação) da Franquia: ");
    gets(franquia.nome);

    printf("\n\n\t\t\t\tGerente: ");
    gets(franquia.gerente);

    sprintf(registro, "%i", getLastId("db/franquias/franquias.dat")+1);
    strcat(registro, ". |");
    strcat(registro, franquia.nome);
    strcat(registro, " |");
    strcat(registro, franquia.gerente);
    strcat(registro, " |");
    strcat(registro, "0");

    insert("db/franquias/franquias.dat", registro);

    
    char op = Menu(yesOrNo, "Franquia Cadastrada com sucesso! Deseja Cadastrar outra?");
    void *funcs[3];
    funcs[1] = createFranq;
    funcs[2] = franquias;

    Router(op, (funcs), mainAdminFunc);
}
void listFranq(){
    char straux[50],*str, *ingredients[50];
    int x = 0;
    FILE *arquivo = fopen("db/franquias/franquias.dat", "r");

    clrscr();
    header();
    printf("\n\n\t\t\t|   Franquia    |    Gerente    | QTD.Vendas |\n");
    
    while((fscanf(arquivo,"%[^\n]\n", &straux))!= EOF){
        str = strtok(straux, "|");
        printf("\t\t  ");
        x = 0;
        while(str != NULL)
        {
            if(x == 0){
                printf("  %s |", str);
            }else if(x == 3){
                printf("%-12s|", str);
            }else{
                printf("%-15s|", str);

            }
            str = strtok(NULL, "|");

            x++;
        }
        printf("\n");
    }
}

void listAndOpFranq(){
    char op;
    listFranq();
    printf("\n\n\t\t\t Voltar(S/N): ");
    op = getchar();

    if (tolower(op) == 's'){
        franquias();
    }
    else{
        listAndOpFranq();
    }
}
void editFranq(){
    char registro[50];
    struct franquias
    {
        char nome[50];
        char gerente[50];
        char vendas[5];

    }franquia;
    
    int id = 0;
    listFranq();
    printf("\n\n\t\tDigite o Número referente ao item que deseja alterar: ");
    scanf("%i", &id);
    
    clrscr();
    header();
    while ((getchar()) != '\n'); 

    printf("\n\n\t\t\t\tNovo nome da Franquia: ");
    gets(franquia.nome);

    printf("\n\n\t\t\t\tGerente: ");
    gets(franquia.gerente);

    printf("\n\n\t\t\t\tQuantidade de Vendas:");
    gets(franquia.vendas);

    sprintf(registro, "|");
    strcat(registro, franquia.nome);
    strcat(registro, " |");
    strcat(registro, franquia.gerente);
    strcat(registro, " |");
    strcat(registro, franquia.vendas);

    update("db/franquias/franquias.dat", id, registro);
    
    
    char op = Menu(yesOrNo, "Atualizar outra Franquia(S/N): ");
    void *funcs[3];
    funcs[1] = editFranq;
    funcs[2] = franquias;

    Router(op, (funcs), mainAdminFunc);  
}
void deleteFranq(){
    int id = 0;

    listFranq();
    printf("\n\n\t\tDigite o Número referente ao item que deseja excluir: ");
    scanf("%i", &id);
    update("db/franquias/franquias.dat", id, "---------------------------------------------");
    char op = Menu(yesOrNo, "Excluir outra Franquia(S/N): ");
    void *funcs[3];
    funcs[1] = deleteFranq;
    funcs[2] = franquias;
    Router(op, (funcs), mainAdminFunc);  

}