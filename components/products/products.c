char* yesOrNo[] = {
    "1. Sim",
    "2. Não",
    NULL
};

void estoque();
void mainAdminFunc();

void cadProducts(){
    char registro[50];
    struct produtos
    {
        char nome[50];
        char precoUnit[5];
        char qtd[5];

    }produto;
    
    clrscr();
    header();

    printf("\n\n\t\t\t\tQual produto: ");
    gets(produto.nome);

    printf("\n\n\t\t\t\tQuantidade: ");
    gets(produto.qtd);

    printf("\n\n\t\t\t\tPreço Unitário: R$ ");
    gets(produto.precoUnit);

    sprintf(registro, "%i", getLastId("db/stock/products.dat")+1);
    strcat(registro, ". |");
    strcat(registro, produto.nome);
    strcat(registro, " |");
    strcat(registro, produto.qtd);
    strcat(registro, " |");
    strcat(registro, produto.precoUnit);

    insert("db/stock/products.dat", registro);

    
    char op = Menu(yesOrNo, "Produto Cadastrado com sucesso! Deseja Cadastrar outro?");
    void *funcs[3];
    funcs[1] = cadProducts;
    funcs[2] = estoque;

    Router(op, (funcs), mainAdminFunc);  

}
void listProducts(){
    char straux[50],*str, *ingredients[50];
    int x = 0;
    FILE *arquivo = fopen("db/stock/products.dat", "r");

    clrscr();
    header();
    printf("\n\n\t\t\t|    Produto    |   Quantidade  |Preço Unitário |\n");
    
    while((fscanf(arquivo,"%[^\n]\n", &straux))!= EOF){
        str = strtok(straux, "|");
        printf("\t\t  ");
        x = 0;
        while(str != NULL)
        {
            if(x == 0){
                printf("  %s |", str);
            }else if(x == 3){
                printf("R$ %-12s|", str);
            }else{
                printf("%-15s|", str);

            }
            str = strtok(NULL, "|");

            x++;
        }
        printf("\n");
    }
    
}

void listAndOp(){
    char op;
    listProducts();
    printf("\n\n\t\t\t Voltar(S/N): ");
    op = getchar();

    if (tolower(op) == 's'){
        estoque();
    }
    else{
        listAndOp();
    }
    
}

void updateProduct(){
    char registro[50];
    struct produtos
    {
        char nome[50];
        char precoUnit[5];
        char qtd[5];

    }produto;
    
    int id = 0;
    listProducts();
    printf("\n\n\t\tDigite o Número referente ao item que deseja alterar: ");
    scanf("%i", &id);
    
    clrscr();
    header();
    while ((getchar()) != '\n'); 

    printf("\n\n\t\t\t\tNovo nome do produto: ");
    gets(produto.nome);

    printf("\n\n\t\t\t\tQuantidade: ");
    gets(produto.qtd);

    printf("\n\n\t\t\t\tPreço Unitário: R$ ");
    gets(produto.precoUnit);

    sprintf(registro, "|");
    strcat(registro, produto.nome);
    strcat(registro, " |");
    strcat(registro, produto.qtd);
    strcat(registro, " |");
    strcat(registro, produto.precoUnit);

    update("db/stock/products.dat", id, registro);
    
    
    char op = Menu(yesOrNo, "Atualizar outro Produto(S/N): ");
    void *funcs[3];
    funcs[1] = updateProduct;
    funcs[2] = estoque;

    Router(op, (funcs), mainAdminFunc);  
}

void removeProducts(){
    int id = 0;

    listProducts();
    printf("\n\n\t\tDigite o Número referente ao item que deseja excluir: ");
    scanf("%i", &id);
    update("db/stock/products.dat", id, "--------------------------------------------");
    char op = Menu(yesOrNo, "Excluir outro Produto(S/N): ");
    void *funcs[3];
    funcs[1] = removeProducts;
    funcs[2] = estoque;
    Router(op, (funcs), mainAdminFunc);  

}

