#include "./libs/file/file.c"
#include "./libs/utils/screen.c"
#include "./libs/utils/helpers.c"
#include "./libs/navigation/menu.c"
#include "./libs/login/login.c"
#include "./components/products/products.c"
#include "./components/rh/rh.c"
#include "./components/cardapio/cardapio.c"
#include "./components/franquias/franquias.c"
#include "./components/relatorios/relatorio.c"
#include "./components/pedido/pedido.c"
#include "./components/comentario/comentario.c"
#include "./components/admin/main.c"
#include "./components/employer/main.c"

//Screen
void header();
void footer();
void clrscr(void);

//Helpers
int countLines(char arquivo[]);
int getNumeroVendas(char arquivo[], int id, int casa);
void updateVendas(char arquivo[], int id, int casa);

//Navigation
char Menu(char *opcoes[], char message[]);
void Router(char selected, void (* *funcs)(), void (*voltar)());
void init();

//File
int verificaArquivo(char path[]);
void insert(char arquivo[], char conteudo[]);
int find(char arquivo[], char conteudo[], char *result[]);
int findById(char arquivo[], char id[] , char *result[]);
int idSearch(char arquivo[], int id, char *result[]);
int getLastId(char arquivo[]);
int apagar(char arquivo[], char id[]);
int update(char arquivo[], int id, char newContent[]);

//Login
int verifyLogin(int type, char nome[], char senha[]);
int login();
void loginAdm();
void loginFranq();
void loginClient();
void loginTypesFunc();

//ADMIN
void rh();
void cardAndProducts();
void relatorios();
void mainAdminFunc();

//Products
void cadProducts();
void listProducts();
void listAndOp();
void updateProduct();
void removeProducts();
void estoque();
void franquias();

//RH
void cadFunc();
void listFunc();
void editFunc();
void deleteFunc();
void listAndOpFunc();


//Cardapio
void cadPrato();
void listPrato();
void editPrato();
void removePrato();
void listAndOpCardapio();

//Franquias
void createFranq();
void listFranq();
void editFranq();
void deleteFranq();


//Relatórios
void maisVendida();
void franqMaisVende();
void lucro();
void mediaLucro();

//Pedidos
void fazerPedido();


//Comentários
void fazerComentario();

//Employer
void mainEmployerFunc();


